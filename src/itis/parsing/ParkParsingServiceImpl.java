package itis.parsing;

import itis.parsing.ParkParsingException.ParkValidationError;
import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        BufferedReader bufferedReader;

        List<ParkValidationError> validationErrors = new ArrayList<>();

        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();

        try {
            bufferedReader = new BufferedReader(new FileReader(new File(parkDatafilePath)));

            while (bufferedReader.ready()) {

                String line = bufferedReader.readLine();

                if (line.equals("***")) {
                    continue;
                }

                String[] string = line.split(":");

                keys.add(string[0].substring(1, string[0].length() - 1));

                string[1] = string[1].trim().replaceAll("\"", "");
                values.add(string[1]);

            }

        } catch (IOException e) {
            throw new IllegalAccessError("Неправильнйы путь или нет файла");
        }

        Park park = null;
        Class<?> classPark = Park.class;

        try {
            Constructor constructor = classPark.getDeclaredConstructor();
            constructor.setAccessible(true);

            park = (Park) constructor.newInstance();

            Field[] fields = classPark.getDeclaredFields();

            for (Field field : fields) {

                field.setAccessible(true);

                Annotation[] annotations = field.getAnnotations();

                Annotation annotation = field.getAnnotation(FieldName.class);

                String nameField = field.getName();

                if (annotation != null) {
                    FieldName fieldName = (FieldName) annotation;
                    nameField = fieldName.value();
                }

                int maxLengthInt = 0;
                boolean notBlank = false, maxLengthFlag = false;

                if (field.getAnnotation(NotBlank.class) != null) {
                    notBlank = true;
                }

                MaxLength maxLengthAnnotation = (MaxLength) field.getAnnotation(MaxLength.class);

                if (maxLengthAnnotation != null) {
                    maxLengthInt = maxLengthAnnotation.value();
                    maxLengthFlag = true;
                }

                for (int i = 0; i < keys.size(); i++) {
                    String key = keys.get(i);

                    boolean ans = false;

                    if (key.equals(nameField)) {
                        String value = values.get(i);

                        if ((value.equals("null") || value.equals("")) && notBlank) {
                            validationErrors.add(new ParkValidationError(nameField, "NotBlank"));
                            ans = true;
                        }

                        if (maxLengthInt <= value.length() && maxLengthFlag) {
                            ans = true;
                            validationErrors.add(new ParkValidationError(nameField, "MaxLength"));
                        }

                        if (ans) {
                            continue;
                        }

                        if (field.getType() == LocalDate.class) {
                            field.set(park, LocalDate.parse(value));
                        } else {
                            field.set(park, value);
                        }

                        break;
                    }
                }


            }

        } catch (Exception e) {
            throw new IllegalAccessError(e.toString());
        }

        if (validationErrors.size() > 0) {
            throw new IllegalArgumentException(
                    new ParkParsingException("не соблюдаются ограничения аннотаций", validationErrors));
        }

        return park;
    }
}